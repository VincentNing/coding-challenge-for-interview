﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace HackPrac
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var numInstructions = int.Parse(Console.ReadLine());

                string text = "";
                Stack<string> undoStack = new Stack<string>();


                string[] splitInstr = new string[] { };


                for (var i = 0; i < numInstructions; i++)
                {
                    var currentInstruction = Console.ReadLine();
                    splitInstr = currentInstruction.Split(" ");
                    if (currentInstruction.Contains("print"))
                    {
                        if (int.Parse(splitInstr[1]) > text.Length || int.Parse(splitInstr[1]) <= 0)
                            Console.WriteLine("Cannot find the character position in the string");

                        else
                            for (int x = 0; x < text.ToArray().Length; x++)
                            {
                                if (x + 1 == int.Parse(splitInstr[1]))
                                {
                                    Console.WriteLine(text.ToArray()[x]);
                                }
                            }



                    }
                    else if (currentInstruction.Contains("add"))
                    {

                        text += splitInstr[1];
                        undoStack.Push(text);

                    }
                    else if (currentInstruction.Contains("delete"))
                    {
                        if (int.Parse(splitInstr[1]) > text.Length)
                            Console.WriteLine("The input number exceeded the limit of characters in the string");
                        else
                        {
                            text = text.Substring(0, text.Length - int.Parse(splitInstr[1]));
                            undoStack.Push(text);
                        }


                    }
                    else if (currentInstruction.Contains("undo"))
                    {
                        if (int.Parse(splitInstr[1]) > undoStack.Count)
                            Console.WriteLine("The input number exceeded the the chacnges were made for the string");
                        else
                            for (var p = 0; p < int.Parse(splitInstr[1]); p++)
                            {
                                undoStack.Pop();

                            }

                    }
                    else
                    {
                        Console.WriteLine("Failture of reading instructions!");
                    }

                    text = undoStack.Peek();
                }
                Console.WriteLine(text);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }


        }
    }
}
