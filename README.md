This is a application does the instruction that is giving by user to edit a text;

There are four types of instructions: print, add, delete and undo;

Each instruction has a value follows

"add" has a string value follows that the user wants to add to the text;

"delete" has a number value that the user wants to delete from the end of the text;

"print" has a number value that which characher of the string the user wants to print;

"undo" has a number value that how many steps the user wants to undo;


You sould type in a number at the first input to decide how many instructions that you want to give
After the application reaches the number, the application will be shutted and display the final text;


Sample Input

5

add abc

print 3

delete 2

add abc

undo 2



Sample Output

c

abc